import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {User} from "../../models/user.model";
import {Subject} from "rxjs";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signUpForm: FormGroup;
  users: User[] = [];
  errorMessage: string;
  userSubject = new Subject<User[]>();
  constructor(private  formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) { }
  ngOnInit(): void{
    this.initForm();
  }
  private initForm() {
    this.signUpForm =  this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]],
      lastname: ['', [Validators.required]],
      firstname: ['', [Validators.required]],
      username: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      newsletter: [false]
    });
  }
  onSubmit() {
    const email = this.signUpForm.get('email').value;
    const password = this.signUpForm.get('password').value;
    const lastname = this.signUpForm.get('lastname').value;
    const firstname = this.signUpForm.get('firstname').value;
    const username = this.signUpForm.get('username').value;
    const phone = this.signUpForm.get('phone').value;
    const newsletter = this.signUpForm.get('newsletter').value;
    const newUser =  new User(username, password, lastname, firstname, phone, email, newsletter);
    this.authService.createNewUser(newUser).then(
      () => {
        this.router.navigate(['']);
      },
      (error) => {
        this.errorMessage = error;
      }
    );
  }
  signIn(){
    this.router.navigate(['/auth/signin']);
  }
}
