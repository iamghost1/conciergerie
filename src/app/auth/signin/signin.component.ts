import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from "../../models/user.model";

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  authStatus: boolean;
  user = {} as User;
  submitted: boolean;
  errorMessage: string;
  loginForm: FormGroup;
  loading = false;
  login: any;
  error = '';
  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private routeActived: ActivatedRoute,
              private authService: AuthService
  ) {
    localStorage.clear();
    this.errorMessage = '';
    this.submitted = false;
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required]]
    });


  }
  ngOnInit():void {
  }
  get f(): any{
    return this.loginForm.controls;
  }

  loginRedirect():any {
    this.router.navigate(['']);
  }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    if (this.loginForm.valid) {
      const data = {
        username: this.loginForm.get('username').value,
        password: this.loginForm.get('password').value
      };
      console.log('TOKEN: ', data);
      this.authService._getConfigLogin(JSON.stringify(data)).subscribe((item) => {
        if (item.code === '200' ) {
          localStorage.setItem('TOKEN', item.data);
          console.log('TOKEN: ', item.data);
          this.__getInfoByUser(data.username);
        }
      }, (error1) => {
        // this.notificationService._getErrorsMessageTopLeft('Vos identifiants ne sont pas corrects');
      });

    }

  }
  // tslint:disable-next-line:typedef
  __getInfoByUser(donnee?: any) {
    const data = {
      username: donnee
    };
    this.authService.__onGetUserInfo(data).subscribe((item) => {
      console.log('item item' , donnee);
      if (item.code === 200 ) {
        console.log('item item' , item);
        localStorage.setItem('user', JSON.stringify(item.data));
        localStorage.setItem('users', JSON.stringify(item.data));
        this.loginRedirect();
      }
    }, (error1) => {

    });


  }
  signUp(){
    this.router.navigate(['/auth/signup']);
  }

}
