import { Component } from '@angular/core';
import firebase from 'firebase/app';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor() {
    const firebaseConfig = {
      apiKey: "AIzaSyB5w5ntZlcmZ8yaDQ_iyg1FmZUWwbevJiw",
      authDomain: "conciergerie-282418.firebaseapp.com",
      databaseURL: "https://conciergerie-282418-default-rtdb.firebaseio.com",
      projectId: "conciergerie-282418",
      storageBucket: "conciergerie-282418.appspot.com",
      messagingSenderId: "328020230840",
      appId: "1:328020230840:web:ac3012d9e8757c82d42a4d",
      measurementId: "G-9C7XPWJR0Q"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
  }
}
