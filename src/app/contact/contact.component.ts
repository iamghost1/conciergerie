import { Component, OnInit } from '@angular/core';
import {ContactsService} from '../services/contacts.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Contact} from '../models/contacts.model';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  contactForm: FormGroup;
  constructor(private formBuilder: FormBuilder,
              private contactsService: ContactsService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(){
    this.contactForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      topic: ['', Validators.required],
      message: ['', Validators.required]
    });
  }

  onSaveContact(){
      const name = this.contactForm.get('name').value;
      const email = this.contactForm.get('email').value;
      const phone = this.contactForm.get('phone').value;
      const topic = this.contactForm.get('topic').value;
      const message = this.contactForm.get('message').value;
    const newContact = new Contact(name, email, phone, topic, message);
    this.contactsService.saveContact(newContact);
    this.router.navigate(['/contacts']);
  }
}
