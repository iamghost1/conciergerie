import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import {RouterModule, Routes} from '@angular/router';
import { ImmobiliersComponent } from './immobiliers/immobiliers.component';
import { VehiculesComponent } from './vehicules/vehicules.component';
import { RestaurantsComponent } from './restaurants/restaurants.component';
import { EvenementielComponent } from './evenementiel/evenementiel.component';
import { ContactComponent } from './contact/contact.component';
import { SigninComponent } from './auth/signin/signin.component';
import { SignupComponent } from './auth/signup/signup.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AuthService} from './services/auth.service';
import {AuthGuardService} from './services/auth-guard.service';
import { AuthComponent } from './adm/auth/auth.component';

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'immobiliers', component: ImmobiliersComponent},
  {path: 'vehicules', component: VehiculesComponent},
  {path: 'restaurants', component: RestaurantsComponent},
  {path: 'evenementiel', component: EvenementielComponent},
  {path: 'contacts', component: ContactComponent},
  {path: 'auth/signin', component: SigninComponent},
  {path: 'auth/signup', component: SignupComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ImmobiliersComponent,
    VehiculesComponent,
    RestaurantsComponent,
    EvenementielComponent,
    ContactComponent,
    SigninComponent,
    SignupComponent,
    HomeComponent,
    FooterComponent,
    AuthComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [AuthService, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
