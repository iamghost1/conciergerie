import { Component, OnInit } from '@angular/core';
import {environment} from "../../environments/environment";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Subject} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {Vehicule} from "../models/vehicule.model";
import {Typevehicule} from "../models/typevehicule.model";
import {Partenaire} from "../models/partenaire.model";

@Component({
  selector: 'app-vehicules',
  templateUrl: './vehicules.component.html',
  styleUrls: ['./vehicules.component.css']
})
export class VehiculesComponent implements OnInit {

  urlvehicule = environment.apiserver;
  urlpartenaire = environment.apiserver;
  urltypevehicule = environment.apiserver;
  vehiculeForm: FormGroup;
  vehicules: Vehicule[] = [];
  partenaires: Partenaire[] = [];
  typevehicules: Typevehicule[] = [];
  vehiucleSubject = new Subject<Vehicule[]>();
  constructor(private formBuilder: FormBuilder, private router: Router,
              private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.initForm();
    this.getVehiucleFromServe();
    this.getPartenaireFromServe();
    this.getTypeVehiculeFromServe();
  }
  emitVehiucle(){
    this.vehiucleSubject.next(this.vehicules);
  }

  private initForm() {
    this.vehiculeForm = this.formBuilder.group({
      marque: ['', Validators.required],
      couleur: ['', Validators.required],
      interieur: ['', Validators.required],
      partenaire: ['', Validators.required],
      typevehicule: ['', Validators.required]
    });
  }
  onSaveVehiucle(){
    const marque = this.vehiculeForm.get('marque').value;
    const typevehicule = 'api/' + 'type_vehiucles/' + this.vehiculeForm.get('typevehicule').value.toString();
    const partenaire =  'api/' + 'partenaires/' + this.vehiculeForm.get('partenaire').value.toString();
    const couleur = this.vehiculeForm.get('couleur').value;
    const interieur = this.vehiculeForm.get('interieur').value;
    const newVehiucle = new Vehicule(marque, typevehicule, partenaire, couleur, interieur);
    this.saveVehiculeToServe(newVehiucle);
    this.router.navigate(['/vehicule/list']);
  }
  saveVehiculeToServe(newVehiucle){
    this.httpClient.post(this.urlvehicule + '/' + 'vehicules', newVehiucle)
      .subscribe(
        () => {
          console.log('Enregistrement terminé !');
        },
        (error) => {
          console.log('erreur de sauvegarde !' + error);
        }
      );
    this.emitVehiucle();
  }
  getVehiucleFromServe(){
    this.httpClient.get<any[]>(this.urlvehicule + '/' + 'vehicules')
      .subscribe(
        (response) => {
          this.vehicules = response['hydra:member'];
        },
        (error) => {
          console.log('Erreur de chargement ! ' + error);
        }
      );
  }
  getPartenaireFromServe(){
    this.httpClient.get<any[]>(this.urlpartenaire + '/' + 'partenaires')
      .subscribe(
        (response) => {
          this.partenaires = response['hydra:member'];
        },
        (error) => {
          console.log('Erreur de chargement ! ' + error);
        }
      );
  }

  getTypeVehiculeFromServe(){
    this.httpClient.get<any[]>(this.urltypevehicule + '/' + 'type_vehicules')
      .subscribe(
        (response) => {
          this.typevehicules = response['hydra:member'];
        },
        (error) => {
          console.log('Erreur de chargement ! ' + error);
        }
      );
  }

}
