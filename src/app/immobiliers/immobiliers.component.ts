import {Component, Input, OnInit} from '@angular/core';
import {Typeimmobilier} from "../models/typeimmobilier.model";
import {Immobilier} from "../models/immobilier.model";
import {Subject} from "rxjs";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-immobiliers',
  templateUrl: './immobiliers.component.html',
  styleUrls: ['./immobiliers.component.css']
})
export class ImmobiliersComponent implements OnInit {

  @Input() id: number;
  urlimmobilier = environment.apiserver;
  urlpartenaire = environment.apiserver;
  urltypeimmobilier = environment.apiserver;
  immobilierForm: FormGroup;
  immobiliers: Immobilier[] = [];
  partenaires = [];
  typeimmobiliers: Typeimmobilier[] = [];
  immobilierSubject = new Subject<Immobilier[]>();
  constructor(private formBuilder: FormBuilder, private router: Router,
              private httpClient: HttpClient) { }


  ngOnInit(): void {
    this.initForm();
    this.getImmobilierFromServe();
    this.getPartenaireFromServe();
    this.getTypeimmobilierFromServe();
  }
  emitImmobilier(){
    this.immobilierSubject.next(this.immobiliers);
  }

  private initForm() {
    this.immobilierForm = this.formBuilder.group({
      title: ['', Validators.required],
      typeimmobilier: ['', Validators.required],
      partenaire: ['', Validators.required],
      parking: [false],
      restaurant: [false],
      roomservices: [false],
      wifi: [false],
      autrecomodite: ['']
    });
  }
  onSaveImmobilier(){
    const title = this.immobilierForm.get('title').value;
    const typeimmobilier = 'api/' + 'type_immobiliers/' + this.immobilierForm.get('typeimmobilier').value.toString();
    const partenaire =  'api/' + 'partenaires/' + this.immobilierForm.get('partenaire').value.toString();
    const parking = this.immobilierForm.get('parking').value;
    const restaurant = this.immobilierForm.get('restaurant').value;
    const roomservices = this.immobilierForm.get('roomservices').value;
    const wifi = this.immobilierForm.get('wifi').value;
    const autrecomodite = this.immobilierForm.get('autrecomodite').value;
    const newImmobilier = new Immobilier(title, typeimmobilier, partenaire, parking, restaurant, roomservices, wifi, autrecomodite);
    this.saveImmobilierToServe(newImmobilier);
    this.router.navigate(['/immobilier/list']);
  }
  saveImmobilierToServe(newImmobilier){
    this.httpClient.post(this.urlimmobilier + '/' + 'immobiliers', newImmobilier)
      .subscribe(
        () => {
          console.log('Enregistrement terminé !');
          this.getImmobilierFromServe();
        },
        (error) => {
          console.log('erreur de sauvegarde !' + error);
        }
      );
    this.emitImmobilier();
  }
  getImmobilierFromServe(){
    this.httpClient.get<any[]>(this.urlimmobilier + '/' + 'immobiliers')
      .subscribe(
        (response) => {
          // console.log('data list retour :', response);
          this.immobiliers = response['hydra:member'];
          console.log(response['hydra:member']);
          this.emitImmobilier();
        },
        (error) => {
          console.log('Erreur de chargement ! ' + error);
        }
      );
  }
  getPartenaireFromServe(){
    this.httpClient.get<any[]>(this.urlpartenaire + '/' + 'partenaires')
      .subscribe(
        (response) => {
          this.partenaires = response['hydra:member'];
        },
        (error) => {
          console.log('Erreur de chargement ! ' + error);
        }
      );
  }

  getTypeimmobilierFromServe(){
    this.httpClient.get<any[]>(this.urltypeimmobilier + '/' + 'type_immobiliers')
      .subscribe(
        (response) => {
          // console.log('data list retour :', response);
          this.typeimmobiliers = response['hydra:member'];
        },
        (error) => {
          console.log('Erreur de chargement ! ' + error);
        }
      );
  }
}
