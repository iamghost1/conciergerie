import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImmobiliersComponent } from './immobiliers.component';

describe('ImmobiliersComponent', () => {
  let component: ImmobiliersComponent;
  let fixture: ComponentFixture<ImmobiliersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImmobiliersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImmobiliersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
