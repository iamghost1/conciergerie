import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImmobiliersRoutingModule } from './immobiliers-routing.module';
import {ImmobiliersComponent} from "./immobiliers.component";
import {NgSelectModule} from "@ng-select/ng-select";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    ImmobiliersComponent
  ],
  imports: [
    CommonModule,
    ImmobiliersRoutingModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ImmobiliersModule { }
