import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import firebase from 'firebase';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isAuth: boolean;
  signInForm: FormGroup;
  errorMessage: string;
  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) { }

  ngOnInit(){
    firebase.auth().onAuthStateChanged(
      (user) => {
        if (user){
          this.isAuth = true;
        }else {
          this.isAuth = false;
        }
      }
    );
  }

  onSignOut()
  {
    this.authService.signOutUser();
  }
  onLoginPage(){
    this.router.navigate(['/auth/signin']);
  }

}
