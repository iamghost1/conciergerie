export class Immobilier{
  constructor(public title: string, public typeimmobilier: string, public partenaire: string, public parking: boolean,
              public restaurant: boolean, public roomservices: boolean, public wifi: boolean, public autrecomodite: string) {
  }
}
