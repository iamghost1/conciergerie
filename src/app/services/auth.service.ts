import { Injectable } from '@angular/core';
import firebase from 'firebase/app';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {User} from "../models/user.model";
import {Observable} from "rxjs";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  urlUser = environment.apiserver;
  user: any;
  private currentUser: Observable<User>;
  constructor(private httpClient: HttpClient, private router: Router, private api) { }

  createNewUser(newUser){
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.urlUser + '/' + 'users', newUser).subscribe(
        () => {
          resolve();
        },
        (error) => {
          reject(error);
        });
    });
  }
  _getConfigLogin(data: any): Observable<any> {
    // @ts-ignore
    return this.api._post(`authenticate`, data, httpOptions)
      .map((response: any) => response)
      .catch(this.handleError);
  }

  _getRegisterInfo(data: any): Observable<any> {
    return this.api._post(`info/getUserVerification`, data)
      .map((response: any) => response)
      .catch(this.handleError);
  }
  _getRegistered(data: any): Observable<any> {
    return this.api._post(`auth/signup`, data)
      .map((response: any) => response)
      .catch(this.handleError);
  }
  __onGetUserInfo(data?: any) {
    return this.api._post(`userInfo`, data)
      .map((response: any) => response)
      .catch(this.handleError);
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.log(errMsg);
    return Observable.throw(errMsg);
  }

  signOutUser(){
    firebase.auth().signOut();
  }
}
