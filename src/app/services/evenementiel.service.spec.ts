import { TestBed } from '@angular/core/testing';

import { EvenementielService } from './evenementiel.service';

describe('EvenementielService', () => {
  let service: EvenementielService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EvenementielService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
