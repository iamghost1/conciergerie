import { Injectable } from '@angular/core';
import {Contact} from '../models/contacts.model';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  contacts: Contact[] = [];
  contactsSubject = new Subject<Contact[]>();
  constructor(private httpClient: HttpClient, private router: Router) { }
  emitContact(){
    this.contactsSubject.next(this.contacts);
    console.log(this.contacts);
  }
  saveContact(newContact){
    this.httpClient.post('http://127.0.0.1:8000/api/contacts.json', this.contacts)
      .subscribe(
        () => {
          console.log('Enregistrement terminé !');
        },
        (error) => {
          console.log('erreur de sauvegarde !' + error);
        }
      );
    this.router.navigate(['/contacts']);
  }
}
