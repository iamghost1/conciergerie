import { TestBed } from '@angular/core/testing';

import { ImmobiliersService } from './immobiliers.service';

describe('ImmobiliersService', () => {
  let service: ImmobiliersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ImmobiliersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
